import { Feed } from './../models/feed';
export const DEFAULT_FEEDS: Feed[] = [
	{ name: "Android Developers Blog", url: "https://android-developers.blogspot.com/atom.xml" },
	{ name: "Cordova Blog", url: "https://cordova.apache.org/feed.xml" },
	{ name: "Windows Developer Blog", url: "https://blogs.windows.com/feed/" }
];