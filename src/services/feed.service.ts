import { DEFAULT_FEEDS } from './../constants/feeds.constants';
import { Feed } from './../models/feed';
import { Injectable } from "@angular/core";

@Injectable()
export class FeedService {

	public getFeeds(): Feed[] {
		return DEFAULT_FEEDS;
	}

}