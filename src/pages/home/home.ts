import { FeedService } from './../../services/feed.service';
import { Feed } from './../../models/feed';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public feeds: Feed[];

  constructor(public navCtrl: NavController, public feedService: FeedService) { }

  public ionViewDidLoad() {
    this.feeds = this.feedService.getFeeds();
  }

}
