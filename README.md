# IEEE MCOE Ionic Assessment
This project is inteded to assess a developer/development team's proficiency in mobile hybrid development within the Ionic ecosystem.

Provided within this project is a starter Ionic template with limited functionality.

## Objective
The objective of this assessment is to build a working iOS and Android application using the Ionic framework that meets the requirements listed below.

## Use-Case
The *IEEE RSS Aggregator* is an application that aggregates RSS feeds. It allows users to view all their RSS feeds in one central location.

When users download the application, they are provided with default feeds to start their RSS aggregation. Users can unsubscribe/resubscribe from these feeds freely. Additionally, users are able to add/remove their own RSS feeds, via URL.

The application should persist the list of the user's RSS feeds on the device locally.

### Requirements
1. The app should contain a view that displays an aggregated view of the user's RSS feeds. It should display the latest 5 articles from each feed sorted by date. This view __should not__ separate articles by feed.
2. The app should contain a view that displays the user's RSS feeds, with the ability to remove them. Users __should not__ be able to remove the feeds packaged with the app by default. However, they can disable (unsubscribe) from them.
3. Users should be able to add their own feeds by providing a URL as well as a display name for the feed.
4. The user's list of RSS feeds should be stored on the device locally.


### Measurements
The assessment will be measured by the following:

1. Code Quality
2. User Experience
3. Solution Approach






